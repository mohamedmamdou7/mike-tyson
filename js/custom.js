/**
 * Created by Bakkar on 4/26/2016.
 */
$(document).ready(function(){
    $("#mob_filter").click(function () {
        $("#filter_menu").slideToggle("fast");
        $("#filter_icon").removeClass("fa-plus");
        $("#filter_icon").addClass("fa-minus");
    });
});
